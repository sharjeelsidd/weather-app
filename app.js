const geocode = require("./util/geocode")
const forecast = require("./util/forecast")

const address = process.argv[2]
console.log(address)

if(!address){
    console.log('Please Enter an address')
}
else{
    geocode(address, (error, {latitude, longitude, location}) => {
        if(error){
            return console.log(error)
        }
        forecast(latitude, longitude, (error, forecastData) => {
            //    console.log('Error', error)
            if(error){
                return console.log(error)
            }
            console.log(location)
            console.log(forecastData)
            
            })
    })
    
}
