const imei_bulk_btn = document.getElementById('btnImei-bulk')
imei_bulk_btn.addEventListener('click', (e)=>{
    e.preventDefault()
    const imei_bulki = document.getElementById("imei-bulk").value
    let imeibulk = imei_bulki.replaceAll(' ', '');
     imeibulk = imeibulk.split(',')
     if(imeibulk.length > 15){
         alert("Enter less than 15 numbers")
         return
     }
    const pArray = []
     imeibulk.forEach(imei => {
         pArray.push(imei_bulk(imei))
         
     });
    Promise.all(pArray).then((resultArray)=> {    
        const divbulk = document.getElementById("result") 
        for (const result of resultArray) {
           let p = document.createElement("p")
            p.textContent = result
            divbulk.appendChild(p)
        }
    })
    console.log(imeibulk)
})

function imei_bulk(imei) {
    return fetch('/imei?imei='+imei)
    .then((response) => {  
        return response.json().then((data)=>{
            if(data.error){
                return "Imei: "+imei+ " " + data.error
            }else{            
                if(data.status == 'error'){
                    //loadBalance(-1);
                }
                return "Imei: "+imei+ " " + data.imeiResponse
            }
        })
    })
}