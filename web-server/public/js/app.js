const imeiForm = document.querySelector('form')
// const tfImei = document.getElementById("tfImei").value;
const wallet = document.querySelector('#wallet')
const messageOne = document.querySelector('#message-1')
const messageTwo = document.querySelector('#message-2')


 

imeiForm.addEventListener('submit', (e)=>{
    e.preventDefault()
    const imei = document.getElementById("tfImei").value
    messageOne.textContent = "Loading!!!";
    fetch('/imei?imei='+imei).then((response) => {
        response.json().then((data)=>{
            if(data.error){
                messageOne.textContent = ""
                messageTwo.textContent = data.error
            }else{
                messageTwo.textContent = data.imeiResponse
                if(data.status == 'error'){
                    loadBalance(-1);
                }
            }
        })
    })
})

const imei_bulk_btn = document.getElementById('btnImei-bulk')
imei_bulk_btn.addEventListener('click', (e)=>{
    e.preventDefault()
    const imei_bulk = document.getElementById("imei-bulk").value
    alert(imei_bulk)
})

var submitBalance = document.getElementById("submitBalance");
var textfieldBalance = document.getElementById("textfieldBalance");
submitBalance.addEventListener('click', (e)=>{
    e.preventDefault()
    const wal = parseInt(textfieldBalance.value);
    if(wal < 1){
        alert('Please enter a number')
    }else{
        loadBalance(wal);
        
    }
})

function loadBalance(wal){
    let balance = localStorage.getItem("imei_balance");
        let balance_copy = document.getElementById("balance_copy");
        if(balance == undefined || balance == null){
            balance = 0;
        }
        const newBalance = parseInt(balance) + wal;
        localStorage.setItem("imei_balance", newBalance);
        balance_copy.innerText = "You currently have $" + newBalance;
}

loadBalance(0);