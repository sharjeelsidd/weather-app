const request = require('request')

const geocode = (address, callback) => {

    // const ur = 'https://sickw.com/api.php?format=JSON&key=8WZ-4IT-G5R-UUX-FCC-KHB-DB2-W3O&imei=' +address+ '&service=3'

   
    const url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/' + address + '.json?access_token=pk.eyJ1Ijoic2hhcmplZWxzaWRkIiwiYSI6ImNrNzVpcnBpMDB6OGMzZXBucHU2dDFldXUifQ.qF90X3r0sohXA4zn1QO-_A'

    request({url, json: true }, (error, { body }) => {
        if(error){
            callback("Unable to connect to IMEI service", undefined)
        }else if(body.features.length === 0) {
            callback("Unable to find IMEI number", undefined)
        }else{
            callback(undefined, {
                latitude: body.features[0].center[1],
                longitude: body.features[0].center[0],
                location: body.features[0].place_name
            })
        }
    })
}

module.exports = geocode
