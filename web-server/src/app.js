const express = require('express')
var bodyParser = require('body-parser')
const app = express()
const port = process.env.PORT || 3000

const path = require('path')
const hbs = require('hbs')
const geocode = require('./util/geocode')
const imeiSearch = require('./util/imei')
const request = require('request')

var jsonParser = bodyParser.json()
var urlencodedParser = bodyParser.urlencoded({ extended: false })

console.log(__dirname)
console.log(__filename)


//Define path to express config
const pathDirectory = path.join(__dirname, '../public')
const viewsPath = path.join(__dirname, '../templates/views')
const partialPath = path.join(__dirname, '../templates/partials')

// Setup Handlebars engine and views location 
app.set('view engine', 'hbs')
app.set('views', viewsPath)
hbs.registerPartials(partialPath)

app.use(express.static(pathDirectory))

// Setup static directory to serve 
app.get('/', (req, res) => {
    res.render('index', {
        title: "IMEI App",
        name: "Sharjeel",
        balance: ''
    })
})


app.get('/bulk', (req,res) => {
    res.render('bulk', {
        title: 'Bulk Page',
        name: 'Sharjeel'
    })
})


// app.get('/help', (req,res) => {
//     res.send([{
//         name: 'Sharjeel',
//         age: 25
//     }, {
//         name: 'Rahil',
//         age: 28
//     }])
// })

app.get('/history', (req,res) => {
    res.render('history', {
        title: "Order History",
        name: 'history'
    })
})



app.get('/imei', (req,res) => {
    if(!req.query.imei){
        return res.send({
            error: 'You must provide a IMEI number'
        })
    }
    console.log("=====> req object: ", req.query.imei);
    imeiSearch(req.query.imei, (error, response) => {
        if(error){
            return res.send({error})
        }
        res.send({
            imeiResponse: response.result,
            imei: req.query.imei,
            status: response.status
        })
     
    });
})

app.get('/help/*', (req,res) =>{
    res.render('404', {
        title: 'Helper',
        name: 'Sharjeel',
        error: 'Helper not Found'
    })
})

app.get('*', (req, res) =>{
    res.render('404', {
        name: 'Sharjeel',
        title: '404 Error',
        error: 'The page does not exists'
    })
})
app.listen(port, () => {
    console.log('Listening...'+port)
})


